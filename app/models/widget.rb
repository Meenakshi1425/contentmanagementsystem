class Widget < ApplicationRecord
  belongs_to :header
  has_many :sections
end
