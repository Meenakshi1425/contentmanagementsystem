class BannerController < ApplicationController
    def edit
        @banner = Banner.last
    end

    def update
        @banner = Banner.last
        @cms = Theme.find(params[:cm_id])
        @widget = Theme.find(params[:cm_id])
        if @banner.update_attributes(set_params)
            redirect_to cm_widget_path(@cms, @widget)
        else
            render :edit
        end
    end

    private
    def set_params
        params.require(:banner).permit(:image, :body)
    end
end
