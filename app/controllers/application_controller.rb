class ApplicationController < ActionController::Base
    before_action :widget

    private
    def widget
        if (@widget = Widget.find_by_published(true))
            @header = Header.find(@widget.header_id)
            @banner = Banner.last
            @menus = @header.menus
            @logo = @header.logo
            @renderer = ERB.new(@widget.body)
            @hbody = ERB.new(@widget.hbody)
            @detail = ERB.new(@widget.detail)
        end
    end
end
