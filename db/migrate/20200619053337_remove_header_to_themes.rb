class RemoveHeaderToThemes < ActiveRecord::Migration[5.2]
  def change
    remove_column :themes, :header, :string 
  end
end
